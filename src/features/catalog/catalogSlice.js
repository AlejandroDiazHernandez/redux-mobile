import { createSlice } from '@reduxjs/toolkit';

const catalogSlice = createSlice({
  name: 'catalog',
  // Se usa para arrancar la App, para seleccionarlos usaremos state.catalog...
  initialState: {
    mobiles: [],
    lastAction: null,
  },
  reducers: {
    // state es el estado de mi slice/feature cuando se llama a este reducer
    doAddMobiles: (state, action) => {
      // En action.payload tengo lo que se manda como argumento al reducer
      const mobiles = action.payload;
      state.mobiles = state.mobiles.concat(mobiles);
    },
    doCreateMobile: (state, action) => {
      const newMobile = action.payload;
      state.mobiles.push(newMobile);
      state.lastAction = 'created';
    },
    doDeleteMobile: (state, action) => {
      const id = action.payload;
      state.mobiles = state.mobiles.filter((mobile) => mobile.id !== id);
      state.lastAction = 'deleted';
    },
  },
});

// Exporto los reducers por separado como actions
export const {
  doAddMobiles,
  doCreateMobile,
  doDeleteMobile,
} = catalogSlice.actions;

// Los selectores permiten sacar información de la store
export const selectMobiles = (state) => state.catalog.mobiles;
export const selectMobilesLength = (state) => state.catalog.mobiles.length;
export const selectLastAction = (state) => state.catalog.lastAction;

// Exportamos default el reducer para usarlo en nuestra store
export default catalogSlice.reducer;
