import React from 'react';
import { useDispatch } from 'react-redux';
import { doDeleteMobile } from '../catalogSlice';

// props también puede destructurarse directamente en la función con:
// export default function MobileCard({ name, brand, description, price }) {}
export default function MobileCard(props) {
  // Destructuring de objetos
  const { id, name, brand, description, price } = props;

  const dispatch = useDispatch();

  function handleDelete() {
    dispatch(doDeleteMobile(id));
  }

  return (
    <li>
      <h3>{name}</h3>
      <p>Brand: {brand}</p>
      <p>{description}</p>
      <p>Price: {price}€</p>

      <button onClick={handleDelete}>Delete</button>
    </li>
  );
}
