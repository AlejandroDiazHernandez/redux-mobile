import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { v4 as uuid } from 'uuid';

import mobileForm from '../../../mobileForm.json';
import { doCreateMobile } from '../catalogSlice';

// Esta función devuelve un objeto que simula el estado inicial dado un array de inputs
function createInitialFormState(inputs) {
  const initialState = {};

  for (let i = 0; i < inputs.length; i++) {
    const inputObject = inputs[i];
    const name = inputObject.name; // name, brand, price...

    initialState[name] = '';
  }

  return initialState;
}

export default function MobileForm(props) {
  const dispatch = useDispatch();
  const [formValues, setFormValues] = useState(null);

  useEffect(() => {
    const initialState = createInitialFormState(mobileForm);
    setFormValues(initialState);
  }, []);

  function handleSubmit(ev) {
    ev.preventDefault();

    dispatch(
      doCreateMobile({
        ...formValues,
        id: formValues.id || uuid(),
      })
    );

    props.history.push('/');
  }

  function handleChangeInput(ev) {
    const { name, value } = ev.target;

    setFormValues({
      ...formValues,
      [name]: value,
    });
  }

  return (
    <div>
      <Link to="/">Volver al catálogo</Link>

      {formValues ? (
        <form onSubmit={handleSubmit}>
          {mobileForm.map((input) => (
            <label htmlFor={input.name} key={input.name}>
              <p>{input.label}</p>

              <input
                name={input.name}
                type={input.type}
                required={Boolean(input.required)}
                value={formValues[input.name]}
                onChange={handleChangeInput}
              />
            </label>
          ))}

          <button style={{ margin: '0 auto', display: 'block' }} type="submit">
            Crear
          </button>
        </form>
      ) : null}
    </div>
  );
}
