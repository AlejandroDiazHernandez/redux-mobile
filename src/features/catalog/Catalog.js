import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { toast } from 'react-toastify';

import MobileList from './components/MobileList';
import MobileForm from './components/MobileForm';

import {
  doAddMobiles,
  selectLastAction,
  selectMobilesLength,
} from './catalogSlice';
import { fakeFetch } from '../../utils/fakeFetch';

export default function Catalog() {
  const dispatch = useDispatch(); // Siempre se recoge dispatch con useDispatch
  const mobilesLength = useSelector(selectMobilesLength);
  const lastAction = useSelector(selectLastAction);

  useEffect(() => {
    if (lastAction === 'created') {
      toast.success('Has creado un nuevo móvil! 📱');
    } else if (lastAction === 'deleted') {
      toast.warn('Has borrado un móvil! 🔥❌');
    }
  }, [lastAction, mobilesLength]);

  useEffect(() => {
    fakeFetch().then((apiMobiles) => {
      // Lanzo la acción que he exportado y le paso los móviles de la API
      dispatch(doAddMobiles(apiMobiles));
    });
  }, []);

  return (
    <div>
      <Switch>
        <Route exact path="/new" component={MobileForm} />
        <Route exact path="/" component={MobileList} />
      </Switch>
    </div>
  );
}
