import React from 'react';
import ReactDOM from 'react-dom';
// Es un Provider parecido al de Context que recibe un store en lugar de value
import { Provider } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';

import App from './App';
import store from './app/store';

import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
